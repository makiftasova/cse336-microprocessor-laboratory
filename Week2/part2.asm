;*****************************************************************
;* This stationery serves as the framework for a                 *
;* user application (single file, absolute assembly application) *
;* For a more comprehensive program that                         *
;* demonstrates the more advanced functionality of this          *
;* processor, please see the demonstration applications          *
;* located in the examples subdirectory of the                   *
;* Freescale CodeWarrior for the HC12 Program directory          *
;*****************************************************************

;Gebze Institute of Technology Computer Science Department
;CSE336 Microprocessor Laboratory, Spring 2014
;
;Week 2
;Assembly code for Dragon12 Board (MC9S12DP256B) which calculates
; first 12 elements of Fibonacci Numbers
;
; 111044016 Mehmet Akif TAŞOVA
; 111044073 Semih Bertuğ Güney


; export symbols
            XDEF Entry, _Startup            ; export 'Entry' symbol
            ABSENTRY Entry        ; for absolute assembly: mark this as application entry point



; Include derivative-specific definitions 
		INCLUDE 'derivative.inc' 

ROMStart    EQU  $2000  ; absolute address to place my code/constant data
						; using this address is forced by course labwork requirements

; variable/data section

            ORG RAMStart
 ; Insert here your data definition.
            ORG $1453 ;jump to memory address 1453
            DC.B $00  ;first 2 of fibonacci numbers
            DC.B $01
            DS.B 10 ; reserve 10 bytes of memory for rest of sequence


; code section
            ORG   ROMStart


Entry:
_Startup:
            LDS   #RAMEnd+1       ; initialize the stack pointer

            CLI                     ; enable interrupts
mainLoop:

            LDAA $1453  			;calculate 3rd number
            ADDA $1454
            STAA $1455
            
            ADDA $1454				;calculate 4th number an so on
            STAA $1456
            
            ADDA $1455
            STAA $1457
            
            ADDA $1456
            STAA $1458
            
            ADDA $1457
            STAA $1459
            
            ADDA $1458
            STAA $145A
            
            ADDA $1459
            STAA $145B
            
            ADDA $145A
            STAA $145C
            
            ADDA $145B
            STAA $145D
            
            ADDA $145C
            STAA $145E


endProg:     STOP                 ; end program

;**************************************************************
;*                 Interrupt Vectors                          *
;**************************************************************
            ORG   $FFFE
            DC.W  Entry           ; Reset Vector
