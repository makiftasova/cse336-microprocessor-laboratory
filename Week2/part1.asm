;*****************************************************************
;* This stationery serves as the framework for a                 *
;* user application (single file, absolute assembly application) *
;* For a more comprehensive program that                         *
;* demonstrates the more advanced functionality of this          *
;* processor, please see the demonstration applications          *
;* located in the examples subdirectory of the                   *
;* Freescale CodeWarrior for the HC12 Program directory          *
;*****************************************************************

;Gebze Institute of Technology Computer Science Department
;CSE336 Microprocessor Laboratory, Spring 2014 
;Week 2
;
;Assembly code for Dragon12 Board (MC9S12DP256B) which places first student's number 
; and reverse of second student's number into memory by given constrains
; thens sums them as requested by course instructor. Each digit of numbers should be 
; threated as one byte
;
; 111044016 Mehmet Akif TAŞOVA
; 111044073 Semih Bertuğ Güney

; export symbols
            XDEF Entry, _Startup            ; export 'Entry' symbol
            ABSENTRY Entry        ; for absolute assembly: mark this as application entry point



; Include derivative-specific definitions 
		INCLUDE 'derivative.inc' 

ROMStart    EQU  $2000  ; absolute address to place my code/constant data

; variable/data section

            ORG RAMStart
 ; Insert here your data definition.
;Counter     DS.W 1
;FiboRes     DS.W 1


            ORG $3000   ;1st students number begins from memory address 3000
            DC.B $01	; placing number into memory 
            DC.B $01
            DC.B $01
            DC.B $00
            DC.B $04
            DC.B $04
            DC.B $00
            DC.B $01
            DC.B $06
        
 
            ORG $3009   ;2nd students number begins from memory address 3009
            DC.B $03	; placing number into memory 
            DC.B $07
            DC.B $00
            DC.B $04
            DC.B $04
            DC.B $00
            DC.B $01
            DC.B $01
            DC.B $01
            
            ORG $3012	; Exact memory address which should store result
            DS.B 9		; length of student number


; code section
            ORG   ROMStart


Entry:
_Startup:
            LDS   #RAMEnd+1       ; initialize the stack pointer

            CLI                     ; enable interrupts
mainLoop:

          LDAA $3000		;calculate sum of 1st set of bytes then store it into exact memory location
          ADDA $3009
          STAA $3012
          
          LDAA $3010 		;calculate sum of 2nd set of bytes then store it into exact memory location
          ADDA $300A
          STAA $3013
          
          LDAA $3002		;calculate sum of 3rd set of bytes then store it into exact memory location
          ADDA $300B
          STAA $3014
          
          LDAA $3003		;calculate sum of 4th set of bytes then store it into exact memory location
          ADDA $300C
          STAA $3015
          
          LDAA $3004		;calculate sum of 5th set of bytes then store it into exact memory location
          ADDA $300D
          STAA $3016
          
          LDAA $3005		;calculate sum of 6th set of bytes then store it into exact memory location
          ADDA $300E
          STAA $3017
          
          LDAA $3006		;calculate sum of 7th set of bytes then store it into exact memory location
          ADDA $300F
          STAA $3018
          
          LDAA $3007		;calculate sum of 8th set of bytes then store it into exact memory location
          ADDA $3010
          STAA $3019 
                  
          LDAA $3008		;calculate sum of 9th set of bytes then store it into exact memory location
          ADDA $3011
          STAA $301A

            

endProg:     STOP                    ;end program

;**************************************************************
;*                 Interrupt Vectors                          *
;**************************************************************
            ORG   $FFFE
            DC.W  Entry           ; Reset Vector
