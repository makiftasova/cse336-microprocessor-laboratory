;Gebze Institute of Technology Computer Science Department
;CSE336 Microprocessor Laboratory, Spring 2014 
;Week 1
;
;Assembly code for Dragon12 Board (MC9S12DP256B) which manipulates leds on board.
; In this project, firstly last 2 digits of student number will be shown on leds,
; then this value will be incremented by 3 then again will shown on leds.
;
; 111044016 Mehmet Akif TAŞOVA
; 111044073 Semih Bertuğ Güney

; export symbols
             XDEF Entry, _Startup            ; export 'Entry' symbol
             ABSENTRY Entry        ; for absolute assembly: mark this as application entry point

; Include derivative-specific definitions 
		         INCLUDE 'derivative.inc' 

ROMStart     EQU   $4000
                                  
             ORG   RAMStart       ; Data section
             



             ORG   ROMStart       ; Code section

;************************************************************************
Entry:
_Startup:
             LDS   #RAMEnd+1      ; initialize the stack pointer

             CLI                  ; enable interrupts
             

            

;************************************************************************
            


mainLoop:   
            LDAA #$FF   ;store hex FF to accumulator A
            STAA DDRB   ; set DDRB to hex FF to gain access to leds on board
             
             
            ;this part will show student number's last 2 digit as binary value on leds
            LDAA #$10   ; load hex value 10(decimal 16) to accumulator A
            ;LDAA #$49   ;load hex value 49(decimal 73) to accumulator A
            STAA PORTB  ;load value to PORTB so it lights leds properly
             
            ;Thi part adds 3 to current value on leds
            ADDA #$3    ;add 3 to AccumulatorA
            STAA PORTB  ; show on leds      

 

endProg:     STOP                 ; end program

            



                    
;***********************************************************************
             
             ORG   $FFFE
             DC.W  Entry          ; Reset Vector



