/*
 * Author: Mehmet Akif TASOVA <makiftasova@gmail.com>
 *
 * 111044016
 *
 * utils.h - utilities for project 
 */

#ifndef __INCLUDE_UTILS_H__
#define __INCLUDE_UTILS_H__

#include "derivative.h"


/* lcd functions */

void lcd_write_cmd(unsigned char);  /* sends command to lcd */
void lcd_write_data(unsigned char); /* sends data to lcd */

void lcd_init(void);	/* itnitializes lcd driver */
void lcd_reset(void);	/* reset lcd's status and clears screen */

void lcd_putchar(char c); /* sends one char to lcd to print */
void lcd_puts(char* ptr); /* ends a string to lcd to print */
void lcd_putnl(void); 	  /* go to next line of lcd display */
void lcd_putuint(unsigned int u); /* writes given unsigned int to lcd screen up to 16 bit unsigned int */


/* delay functions */
void delay_50us(unsigned int itime);
void delay_1ms(unsigned int itime);

/* keypad functions */
void kp_init(void); /* initializes keypad */
int kp_get_key(void); /* reads keypad until gets an input */

#endif