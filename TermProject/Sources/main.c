/*
 * Author: Mehmet Akif TASOVA <makiftasova@gmail.com>
 *
 * 111044016
 *
 * main.c - main part o project 
 */


#include <hidef.h>        /* common defines and macros */
#include "derivative.h"   /* derivative-specific definitions */
#include "utils.h"        /* keypad and lcd drivers. also includes delay functions */

/* basic logic */
#define FALSE 0
#define TRUE  1

#define DISTANCE 14      // in centimeter
#define GARAGE_LIMIT 10 

#define CLOCK_SHOW_LENGTH 5 //in seconds

#define TIMER_OVF_TIME 87 //in ms  

/* interrups vectror address calculator */
#define INTR_CODE(name) ( ((( 0x10000 - name) / 2) - 1 ) )


#define enable_intr()  __asm(cli)
#define disable_intr() __asm(sei)

#define WAITFOREVER() for(;;) __asm(wai)

/* interrupt routines */
interrupt INTR_CODE(Vrti) void rti_isr(void);
interrupt INTR_CODE(Vtimch1) void tch1_isr(void);
interrupt INTR_CODE(Vtimch2) void tch2_isr(void);

/* global data */
volatile unsigned int rt_hours, rt_minutes, rt_seconds, rt_totalseconds, rt_bool_showtime; /* 24h clock */
volatile unsigned int sens1_time, sens2_time; /* Capture 1 and 2 time */
volatile unsigned int sens1_pass, sens2_pass; /* flags wihch indictes sensor responses */
volatile unsigned int cars_total, cars_total_past, car_speed;


/* initializer functions */
void init_globals(void);  /* initializes global variables */
void init_ports(void);	/* initializes ports used in project */
void rt_init(void); /* initialize realtime system */
void tch_init(void); /* initialize input capture system */

/* helpers */
void rt_print_time(void); /* print 24h clock */
void display_menu(void);  /* displays main menu on lcd */
void bzr_play(unsigned int secs); /* plays buzzer for given amount of time in seconds */



void main(void) 
{
  /* data definitions */
  int key_input = -1;
  unsigned short timebegin;
  
  /* initialize components */
  disable_intr();
  init_globals();
  init_ports();
  lcd_init();
  lcd_reset();
  kp_init();
  rt_init();
  tch_init();
  enable_intr();
  

  for(;;){
  
    
   // __asm(wai);
    
    display_menu();
    key_input = kp_get_key();
    lcd_reset();
    
    
    switch(key_input){
      
      case 1:
        timebegin = rt_totalseconds;
        while((rt_totalseconds - timebegin) <= CLOCK_SHOW_LENGTH){
           rt_print_time();
           delay_1ms(690);
        }
        break;
      case 2:
        lcd_puts("IN/TOTAL: ");
        lcd_putuint(cars_total);
        lcd_putchar('/');
        lcd_putuint(GARAGE_LIMIT);
        lcd_putnl();
        
        if(GARAGE_LIMIT <= cars_total)
          lcd_puts("!!GARAGE FULL!!");
        
        kp_get_key();
        break;
      case 3:
        lcd_puts("AVG NUM: ");
        lcd_putuint(cars_total_past / (rt_totalseconds / 60));
        lcd_putnl();
        lcd_puts("PRESS ANY KEY");
        kp_get_key();
        break;
      default:
        lcd_puts("INVALID");
        lcd_putnl();
        lcd_puts("INPUT");
        break;
     }        
      
  }
  	
}


/* interrupt routines */
interrupt INTR_CODE(Vrti) void rti_isr(void)
{
    static char intr_count = 0;
    
    ++(intr_count);
    
    if(0 == intr_count % 8){    
      ++rt_seconds;
      ++rt_totalseconds;
      intr_count = 0; /* one sec pass */      
    }
    
    if(60 <= rt_seconds){
      rt_seconds = rt_seconds % 60;
      ++rt_minutes; 
    }                                         
    
    if(60 <= rt_minutes){
      rt_minutes = rt_minutes % 60;
      ++rt_hours; 
    }
    
    if((0 == intr_count) && (TRUE == rt_bool_showtime))
      rt_print_time();
    
    CRGFLG = CRGFLG | 0x80; // cleanr interrupt 
}

interrupt INTR_CODE(Vtimch1) void tch1_isr(void) {

	sens1_pass = TRUE;
	if(sens2_pass == TRUE){
    /* CAR OUT */
    lcd_reset();
    lcd_puts(" CAR OUT ");
    lcd_putnl();
    --cars_total;
    ++cars_total_past;
    if(GARAGE_LIMIT <= cars_total)
      lcd_puts("!!GARAGE FULL!!");

    /* car speed calculatipns will be here */
      
    delay_1ms(2000);
    display_menu();
           
    sens1_pass = FALSE;
    sens2_pass = FALSE;   
   }

   TFLG1 = 0x02;  // clear interrupt 
}

interrupt INTR_CODE(Vtimch2) void tch2_isr(void){
   
  unsigned int cycle_diff = 0, time_diff = 0;
  unsigned int begin_sec = rt_seconds;
   
  sens2_pass = TRUE;
  if(sens1_pass == TRUE){
    /* CAR IN */
    lcd_reset();
    lcd_puts(" CAR  IN ");
    lcd_putnl();
    ++cars_total;
    ++cars_total_past;
    if(GARAGE_LIMIT <= cars_total)
      lcd_puts("!!GARAGE FULL!!");
      
    /* car speed calculatipns will be here */
    
    delay_1ms(2000);
    display_menu();
            
    sens2_pass = FALSE;
    sens1_pass = FALSE;     
   }
    
   TFLG1 = 0x04; // clear interrupt 
}

void init_globals(void)
{
  rt_hours = 0;
  rt_minutes = 0;
  rt_seconds = 0;
  rt_bool_showtime = FALSE;
  sens1_time = 0;
  sens2_time = 0;
  cars_total = 0;
  cars_total_past = 0;
  car_speed = 0;
  sens1_pass = FALSE;
  sens2_pass = FALSE;
}

void init_ports(void) 
{
  DDRT = DDRT | 0x20; /* buzzer */   
}

void rt_init(void)
{
   RTICTL = 0x6F; /* 1 sec for 8 interrupts */
   CRGINT = CRGINT | 0x80;
   CRGFLG = CRGFLG | 0x80;
   
   lcd_puts("RTI ENABLED");
   lcd_putnl();
   lcd_reset();
}

void tch_init(void){
  /* code here*/
  TSCR1 = 0x80;
  TSCR2 = 0x05; /* set prescaler to 87.38ms */

  /* Setup for IC1 */ 
  TIOS = TIOS & ~0x02; /* Configure PT1 as IC */ 
  TCTL4 = (TCTL4 | 0x04) & ~0x08; /* Capture Rising Edge */ 
  TFLG1 = 0x02; /* Clear IC1 Flag */ 

  TIE = TIE | 0x02; /* Enable IC1 Interrupt */ 
 
  /* Setup for IC2 */ 
  TIOS = TIOS & ~0x04; /* Configure PT2 as IC */ 
  TCTL4 = (TCTL4 | 0x10) & ~0x20; /* Capture Rising Edge */ 
  TFLG1 = 0x04; /* Clear IC2 Flag */ 
 
  /* Set interrupt vector for Timer Channel 2 */ 
  TIE = TIE | 0x04; /* Enable IC2 Interrupt */
  
  lcd_puts("TCH ENABLED");
  lcd_putnl();
  lcd_reset(); 
}

void rt_print_time(void)
{
  lcd_reset();
  lcd_putuint(rt_hours);
  lcd_putchar(':');
  lcd_putuint(rt_minutes);
  lcd_putchar(':');
  lcd_putuint(rt_seconds);
  lcd_putnl();    
}

void display_menu(void) 
{
  lcd_reset();
  lcd_puts("1-TIME  2-STATUS");
  lcd_putnl();
  lcd_puts("3-AVG # OF CARS");
}

void bzr_play(unsigned int secs)
 {
  unsigned int bgn_time = 0;
  
  PTT = PTT | 0x20;
  bgn_time = rt_seconds;
  while((rt_seconds - bgn_time) < secs )
        ;
  PTT = PTT & 0xDF;
}