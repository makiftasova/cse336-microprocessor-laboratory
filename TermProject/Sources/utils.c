/*
 * Author: Mehmet Akif TASOVA <makiftasova@gmail.com>
 *
 * 111044016
 *
 * utils.c - implementations of utilities for project 
 */

#include "utils.h"
#include "derivative.h"      /* derivative-specific definitions */

/* lcd specific defines */ 
#define LCD_DATA PORTK
#define LCD_CTRL PORTK
#define RS 0x01
#define EN 0x02

/* lcd functions */

void lcd_write_cmd(unsigned char command)
{
  unsigned char x;
  
  x = (command & 0xF0) >> 2;         //shift high nibble to center of byte for Pk5-Pk2
  LCD_DATA =LCD_DATA & ~0x3C;          //clear bits Pk5-Pk2
  LCD_DATA = LCD_DATA | x;          //sends high nibble to PORTK
  delay_1ms(1);
  LCD_CTRL = LCD_CTRL & ~RS;         //set RS to command (RS=0)
  delay_1ms(1);
  LCD_CTRL = LCD_CTRL | EN;          //rais enable
  delay_1ms(5);
  LCD_CTRL = LCD_CTRL & ~EN;         //Drop enable to capture command
  delay_1ms(15);                       //wait
  
  x = (command & 0x0F)<< 2;          // shift low nibble to center of byte for Pk5-Pk2
  LCD_DATA =LCD_DATA & ~0x3C;         //clear bits Pk5-Pk2
  LCD_DATA =LCD_DATA | x;             //send low nibble to PORTK
  LCD_CTRL = LCD_CTRL | EN;          //rais enable
  delay_1ms(5);
  LCD_CTRL = LCD_CTRL & ~EN;         //drop enable to capture command
  delay_1ms(15);
}


void lcd_write_data(unsigned char data)
{
  unsigned char x;
  
  x = (data & 0xF0) >> 2;
  LCD_DATA =LCD_DATA & ~0x3C;                     
  LCD_DATA = LCD_DATA | x;
  delay_1ms(1);
  LCD_CTRL = LCD_CTRL | RS;
  delay_1ms(1);
  LCD_CTRL = LCD_CTRL | EN;
  delay_1ms(1);
  LCD_CTRL = LCD_CTRL & ~EN;
  delay_1ms(5);
       
  x = (data & 0x0F)<< 2;
  LCD_DATA =LCD_DATA & ~0x3C;                     
  LCD_DATA = LCD_DATA | x;
  LCD_CTRL = LCD_CTRL | EN;
  delay_1ms(1);
  LCD_CTRL = LCD_CTRL & ~EN;
  delay_1ms(15);
}


void lcd_init(void) 
{
  DDRK = 0xFF;   
  lcd_write_cmd(0x33);   //reset sequence provided by data sheet
  delay_1ms(1);
  lcd_write_cmd(0x32);   //reset sequence provided by data sheet
  delay_1ms(1);
  lcd_write_cmd(0x28);   //Function set to four bit data length
                                         //2 line, 5 x 7 dot format
}

void lcd_reset(void)
{
  delay_1ms(1);
  lcd_write_cmd(0x06);  //entry mode set, increment, no shift
  delay_1ms(1);
  lcd_write_cmd(0x0E);  //Display set, disp on, cursor on, blink off
  delay_1ms(1);
  lcd_write_cmd(0x01);  //Clear display
  delay_1ms(1);
  lcd_write_cmd(0x80);  //set start posistion, home position
  delay_1ms(1);
}

void lcd_putchar(char c)
{
  lcd_write_data(c);
  delay_50us(1);
}

void lcd_puts(char* ptr) 
{
    while(*ptr != '\0'){
      lcd_putchar(*ptr);
      ++ptr;  
    }
}

void lcd_putnl(void)
{
   lcd_write_cmd(0xC0);
   delay_50us(1);
}

void lcd_putuint(unsigned int u)
{
  short digit, indx = 4, i=0, k=0;
  char buffer[6] = {'0', '0', '0', '0', '0', '\0'};
  
  if(u == 0){
    lcd_putchar('0');
    return;
  }
  
  
  while(u != 0){
    digit = u%10;
    buffer[indx] = (digit + 48);
    --indx;
    u = u / 10;  
  }
  
  while('0' == buffer[i]){
     ++i;
  }
  
  for(k = 0; k < 6; ++k)
    buffer[k] = buffer [k+i];
    
  lcd_puts(buffer);
}

 /* delays */
 
#define D50US 133

void delay_50us(unsigned int itime) 
{
    volatile int c;
    for(; itime > 0; --itime)
      for(c=D50US; c > 0; --c);
}

void delay_1ms(unsigned int itime)
{
  unsigned int i; unsigned int j;
  
  for(i=0;i<itime;i++)
    for(j=0;j<4000;j++);
}


/* keypad specific defines */
#define ROW_NUM 4
#define COL_NUM 4

void kp_init(void)
{
   DDRA = 0x0F; // enable keypad 
}

int kp_get_key(void) 
{
  int row, column;
               
  const char row_mask[ROW_NUM] = { 0xFE,0xFD,0xFB,0xF7 };
  const char col_mask[COL_NUM] = { 0x10,0x20,0x40,0x80 };        
  const unsigned int keys[ROW_NUM][COL_NUM] = { 1,2,3,10,
                                                4,5,6,11,
                                                7,8,9,12,
                                                14,0,15,13 };
    
  for(;;){ // wait until input
      
    for(row=0 ; row < ROW_NUM ; ++row){
         
      PORTA = 0xFF;// Reset PortA
      for(column=0; column < COL_NUM ;++column){
                
        PORTA = PORTA & row_mask[row]; // Set specific row to 0
                                     
        if( (PORTA & col_mask[column]) == 0 ){
        // Check if any key is pressed
           
          delay_1ms(1);                      
          // Wait 1ms and check again for make sure key is pressed.
          if( (PORTA & col_mask[column]) == 0 ){
            return keys[row][column]; 
          }                   
              
        }
           
      }
      
    }
  } 
}