;*****************************************************************
;* This stationery serves as the framework for a                 *
;* user application (single file, absolute assembly application) *
;* For a more comprehensive program that                         *
;* demonstrates the more advanced functionality of this          *
;* processor, please see the demonstration applications          *
;* located in the examples subdirectory of the                   *
;* Freescale CodeWarrior for the HC12 Program directory          *
;*****************************************************************

;Gebze Institute of Technology Computer Science Department
;CSE336 Microprocessor Laboratory, Spring 2014 
;Week 4
;
;Assembly code for Dragon12 Board (MC9S12DP256B) which implements selection sort.
;	It will sorts given arrayn in addresses between $3000 to $300A with
;	selection sort algorithm
;
; 111044016 Mehmet Akif TAŞOVA
; 111044073 Semih Bertuğ Güney


; export symbols
            XDEF Entry, _Startup  ; export 'Entry' symbol
            ABSENTRY Entry        ; for absolute assembly: mark this as application entry point



; Include derivative-specific definitions 
		INCLUDE 'derivative.inc' 

ROMStart    EQU  $4000  ; absolute address to place my code/constant data
STACK       EQU  $2000
DataStart   EQU  $3000
DataEnd     EQU  $300A
LastData    EQU  $3009

; variable/data section

            ORG RAMStart
 ; Insert here your data definition.
MaxCounter  DS.B 1 ; counter for loop of finding max elem
MaxNum      DS.B 1 ; maximum in range
MaxIndex    DS.W 1 ; maximum index

IndexStoreY DS.W 1; store Y index

; Memory Stores for storing Accumulators when Subroutine called
STOREA      DS.B 1
STOREB      DS.B 1
STOREX      DS.W 1
STOREY      DS.W 1

; Place array into memory            
            ORG DataStart
            DC.B $25
            DC.B $32
            DC.B $02
            DC.B $51
            DC.B $10
            DC.B $37
            DC.B $06
            DC.B $14
            DC.B $49
            DC.B $20


; code section
            ORG   ROMStart


Entry:
_Startup:
            LDS   #RAMEnd+1       ; initialize the stack pointer

            CLI                     ; enable interrupts


mainLoop:
            LDS #STACK   	; initialize stack at address $2000
            
            LDX #DataStart 	; set array begin
            LDY #LastData	;set array end
            
            
            PSHX
            PSHY
            JSR  FINDMAX	; find max in array
            PULY
            PULX 			; pull largest values index
            
            
            LDY #$3000		; load first element address to index Y
            
            
            ;put arguments to stack
            PSHX
            PSHY
            JSR SWAP		; swap maximum element with first one
           
            ; free(stack);
            PULY
            PULX
          
  ; Y --> travelling index
  ; X --> biggest index
            INY
            STY IndexStoreY

 ; Begin sorting rest of array          
SortLoop:   CPY #DataEnd
            BEQ SortDone
            
            LDX #LastData
            
            PSHY
            PSHX
            JSR  FINDMAX
            PULY
            PULX ; max index
            
            LDY  IndexStoreY
            
            PSHX
            PSHY
            JSR SWAP
            PULY
            PULX
            
            INY
            STY  IndexStoreY
            
            JMP SortLoop

;done sorting 
SortDone: 
            JMP endProgram

; BEGIN SUBROUTINE FINDMAX
FINDMAX:    STAA  STOREA
            STAB  STOREB
            STX   STOREX
            STY   STOREY
            ; let the subroutine begin
            LDX   4, SP ; Begin Address
            LDY   2, SP ; End Address           
            
            LDAA  X
            STAA  MaxNum
            

maxLoop:    CPX  2, SP
            BEQ  maxLoopEnd
            
            INX
            
            LDAA X
            
            CMPA MaxNum
            BLE   doNothing
    
            STAA MaxNum
            STX  4, SP
           
doNothing:
    
            
            JMP   maxLoop
    
maxLoopEnd:
     
            
            LDY   STOREY
            LDX   STOREX
            LDAB  STOREB
            LDAA  STOREA
            
            RTS
;END SUBROUTINE FINDMAX

;BEGIN SUBROUTINE SWAP
SWAP:      
            STAA  STOREA
            STAB  STOREB
            STX   STOREX
            STY   STOREY
            LDX   4,SP ; num1
            LDY   2,SP ; num2
            LDAA  0, X
            LDAB  0, Y
            STAA  0, Y
            STAB  0, X
           
            LDY   STOREY
            LDX   STOREX
            LDAB  STOREB
            LDAA  STOREA
            
            RTS
;END SUBROUTINE SWAP


 endProgram: SWI ; end program
;**************************************************************
;*                 Interrupt Vectors                          *
;**************************************************************
            ORG   $FFFE
            DC.W  Entry           ; Reset Vector
