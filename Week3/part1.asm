;*****************************************************************
;* This stationery serves as the framework for a                 *
;* user application (single file, absolute assembly application) *
;* For a more comprehensive program that                         *
;* demonstrates the more advanced functionality of this          *
;* processor, please see the demonstration applications          *
;* located in the examples subdirectory of the                   *
;* Freescale CodeWarrior for the HC12 Program directory          *
;*****************************************************************

;Gebze Institute of Technology Computer Science Department
;CSE336 Microprocessor Laboratory, Spring 2014
;
;Week 2
;Assembly code for Dragon12 Board (MC9S12DP256B) which finds
;   minimum in given array
;
; 111044016 Mehmet Akif TAŞOVA
; 111044073 Semih Bertuğ Güney

; export symbols
            XDEF Entry, _Startup            ; export 'Entry' symbol
            ABSENTRY Entry        ; for absolute assembly: mark this as application entry point



; Include derivative-specific definitions 
		INCLUDE 'derivative.inc' 

ROMStart    EQU  $4000  ; absolute address to place my code/constant data
DataStart EQU $3000 ; begin of array

; variable/data section

            ORG RAMStart
 ; Insert here your data definition.
Counter DS.W 1
Current DS.W 1
            
            ORG DataStart
            DC.B  $17
            DC.B  $13 
            DC.B  $15
            DC.B  $04
            DC.B  $12
            DC.B  $07
            DC.B  $03
            DC.B  $04
            DC.B  $20
            DC.B  $05
    

; code section
            ORG   ROMStart


Entry:
_Startup:
            LDS   #RAMEnd+1       ; initialize the stack pointer

            CLI                     ; enable interrupts                                        
mainLoop:
            LDAA #$FF   ;store hex FF to accumulator A  (IMMEDIATE)
            STAA DDRB   ; set DDRB to hex FF to gain access to leds on board
            
            LDAA DataStart
            STAA Current
            
            LDX #$00 ; loop counter
            STX Counter
            
            LDY #DataStart
            
    SearchBegin:
            ;loop body
            LDAA Current
            LDAB Y
            
            CBA
            BLT Pass
            
            STAB Current ;store new lowerst one        
        Pass:
        
            INY      
             
             ;loop control
             LDX Counter               
             INX ;increase counter by one
             STX Counter
             CPX #$0A
             BNE SearchBegin  ;loop once more
             
         STAA Current ; store true lower value
         STAA PORTB ; show on leds
         
            
            
 SearchEnd:
          
            

endProg:     STOP                 ; end program

;**************************************************************
;*                 Interrupt Vectors                          *
;**************************************************************
            ORG   $FFFE
            DC.W  Entry           ; Reset Vector
